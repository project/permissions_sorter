
$(function() {
  permissions_sorter();
});

/**
 * This function sorts the permission rows at the q=admin/user/access table so that
 * permissions belonging to a certain content-type are grouped together.
 */
function permissions_sorter() {

  // Find all the table rows containing the Node module's permissions.
  var nodeRows = [];
  var nodeHeader = $('#module-node').parents('tr:first');
  // Note the `$(obj)' syntax due to jQuery 1.1's DOM traversal being destructive.
  var next = $(nodeHeader).next();
  while ($(next).children().length != 1) {
    nodeRows.push(next.get(0));
    next = next.next()
  }

  // Extract the content-type to which each belongs.
  $.each(nodeRows, function(index, row) {
    var permission = $(row).find('input:first').val();
    if (/^(create|edit own|edit|delete own|delete) (.*) content$/.test(permission)) {
      row.contentType = RegExp.$2;
      row.permission = RegExp.$1;
    } else {
      row.contentType = '';
      row.permission = permission;
    }
  });
  
  // Sort permissions (that is, rows) by conent-type.
  nodeRows.sort(function(a, b) {
    if (a.contentType == b.contentType) {
      if (a.permission < b.permission) return -1;
      if (a.permission > b.permission) return 1;
      return 0;
    }
    if (a.contentType < b.contentType) return -1;
    if (a.contentType > b.contentType) return 1;
    return 0;
  });

  // Re-stripe them.  
  var flip = { 'odd': 'even', 'even': 'odd' };
  var parity = 'odd';
  var prevContentType = '';
  $.each(nodeRows, function(index, row) {
    if (!row.contentType || row.contentType != prevContentType) {
      parity = flip[parity];
    }
    $(row).removeClass('odd').removeClass('even').addClass(parity);
    prevContentType = row.contentType;
  });
  
  // Finally, re-insert the newly sorted rows into the table.
  $(nodeRows).remove();
  nodeHeader.after(nodeRows);
}

